//
//  TestViewModel.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.

#import "TestViewModel.h"

@implementation TestViewModel

+ (JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"name":@"name",@"downStatues":@"downStatues",@"downUrl":@"downUrl",@"progress":@"progress",
                                                       }];
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}
@end
