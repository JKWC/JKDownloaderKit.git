//
//  TestViewModel.h
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestViewModel : JSONModel

/**
 视频名字
 */
@property(nonatomic,strong) NSString *name;

/**
 视频下载的状态：1 已下载；2 下载中；3 还没有下载
 */
@property(nonatomic,strong) NSString *downStatues;

/**
 视频下载的url
 */
@property(nonatomic,strong) NSString *downUrl;

/**
 视频下载的进度
 */
@property(nonatomic,strong) NSString *progress;



@end

NS_ASSUME_NONNULL_END
