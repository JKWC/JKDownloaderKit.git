//
//  JKFilePathExtension.h
//  JKOCFilePathOperation
//
//  Created by 王冲 on 2019/1/24.
//  Copyright © 2019年 希爱欧科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JKFilePathExtension : NSObject

#pragma mark 1、判断文件或文件夹是否存在
+(BOOL)jk_judgeFileOrFolderExists:(NSString *)filePathName;
#pragma mark 2、类方法创建文件目录
/**类方法创建文件夹目录 folderNmae:文件夹的名字*/
+(NSString *)jk_createFolder:(NSString *)folderName;

#pragma mark 3、判断文件目录是否存在，不存在就创建
+(NSString *)jk_checkFilePath:(NSString *)filePath;

#pragma mark 输入文件路径，得到排序好的相关数组，可以设置升序或降序
/**
 输入文件路径，得到排序好的相关数组，可以设置升序或降序
 
 @param path 文件列表路径
 @param isascending 是否升序: YES：升  NO：降
 @return 排序好的数组
 */

+ (NSMutableArray *)jk_fileDirectoryList:(NSString *)path Isascending:(BOOL)isascending;

#pragma mark 路径下所有文件的列表
+ (NSMutableArray *)jk_fileList:(NSString *)path Isascending:(BOOL)isascending;

#pragma mark 离线列表
+ (NSMutableArray *)jk_checkJKDonePath:(NSString *)donePath withDowningPath:(NSString *)downingPath isascending:(BOOL)isascending;

#pragma mark 离线列表
+ (NSMutableArray *)jk_testcheckJKDonePath:(NSString *)donePath withDowningPath:(NSString *)downingPath isascending:(BOOL)isascending;

#pragma mark 计算文件路径下的大小
/**
 计算文件路径下的大小

 @param filePath 资源的路径
 @return 返回资源的大小 long long
 */
+(long long)jk_fileSize:(NSString *)filePath;

#pragma mark 判断文件是否存在
/**
 判断文件是否存在

 @param filePath 文件路径
 @return YES:存在 NO:不存在
 */
+(BOOL)jk_judgeFileExists:(NSString *)filePath;

#pragma mark 移动文件
/**
 移动文件

 @param fromPath 移动的文件路径
 @param toPath 移动到的路径
 */
+(void)moveFile:(NSString *)fromPath toPath:(NSString *)toPath;

#pragma mark 删除路径
/**
 删除路径

 @param filePath 路径
 */
+(void)removeFile:(NSString *)filePath;

@end

NS_ASSUME_NONNULL_END
