//
//  JKDownLoaderManger.h
//  JKDownLoader
//
//  Created by 王冲 on 2019/2/9.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JKDownLoader.h"

NS_ASSUME_NONNULL_BEGIN

@interface JKDownLoaderManger : NSObject

/**
 下载失败的block
 */
@property(nonatomic,strong) downLoadFailBlock fail;

+(instancetype)shareDownloaderManger;

#pragma mark 定义一个下载的方法

/**
 定义一个下载的方法

 @param url 下载的地址
 @param downInfo 下载地址的信息
 @param progress 下载进度
 @param stateType 下载的状态
 @param success 成功
 @param fail 失败
 */
-(void)jk_downLoader:(NSURL *)url withDownInfo:(downLoadInfoBlock)downInfo withProgress:(downLoadProgressChange)progress withdownLoadStateChangeType:(downLoadStateChangeTypeBlock)stateType withSuccess:(downLoadSuccessBlock)success withFail:(downLoadFailBlock)fail;

#pragma mark 暂停某个文件下载

/**
 暂停当前的下载任务

 @param url 资源的url
 */
-(void)pauseLoadWithUrl:(NSURL *)url;

#pragma mark 取消当前的下载任务
    
/**
 取消当前的下载任务
 
 @param url 资源的url
 */
-(void)cancelLoadWithUrl:(NSURL *)url;

#pragma mark 取消并清理当前的下载任务
    
/**
 取消并清理当前的下载任务
 
 @param url 资源的url
 */
-(void)cancelAndCleanLoadWithUrl:(NSURL *)url;

#pragma mark 继续某个文件下载
    
/**
 继续某个文件下载
 
 @param url 资源的url
 */
-(void)resumeLoadWithUrl:(NSURL *)url;

#pragma mark 暂停全部文件下载
    
/**
 暂停全部文件下载
 */
-(void)pauseAllDownLoad;

#pragma mark 开始全部文件下载
    
/**
 开始全部文件下载
 */
-(void)startAllDownLoad;

#pragma mark 获取当前url对应的 DownLoader
    
/**
   传进来一个下载地址的url，返回一个在下载池中 url对应的 JKDownLoader
 */
- (JKDownLoader *)getDownLoaderWithURL:(NSURL *)url;
    

@end

NS_ASSUME_NONNULL_END
