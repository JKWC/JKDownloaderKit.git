//
//  JKDownTool.h
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface JKDownTool : NSObject

#pragma mark 下载的状态
/**
 下载的状态

 @param fileName 文件的名字
 @return 返回视频下载的状态：1 已下载；2 下载中；3 还没有下载
 */
+(NSString *)downStatusFileName:(NSString *)fileName;

@end

NS_ASSUME_NONNULL_END
