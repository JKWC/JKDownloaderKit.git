//
//  JKDownLoader.h
//  JKDownLoader
//
//  Created by 王冲 on 2019/2/8.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// 通知
#define JKDownLoadURLOrStateChangeNotification @"JKDownLoadURLOrStateChangeNotification"

// 下载中的基础路径
#define cachesPathLoading [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/JKDownLoading"]
// 下载完成的基础路径
#define cachesPathCompleted [NSHomeDirectory() stringByAppendingPathComponent:@"/Library/Caches/JKDownloadCompleted"]

// 下载的状态
typedef enum : NSUInteger {
    JKDownLoaderStateUnKnown,
    /** 下载暂停(取消也归为暂停) */
    JKDownLoaderStatePause,// 默认是0，暂停放到第一个很合适
    /** 正在下载 */
    JKGDownLoaderStateDowning,
    /** 已经下载 */
    JKGDownLoaderStateSuccess,
    /** 下载失败 */
    JKGDownLoaderStateFailed
} JKDownLoaderState;

typedef void(^downLoadProgressChange)(float progress);
typedef void(^downLoadInfoBlock)(long long totalSize,NSString *cachePath);
typedef void(^downLoadSuccessBlock)(NSString *cachePath);
typedef void(^downLoadFailBlock)(NSString *errorMessage);
typedef void(^downLoadStateChangeTypeBlock)(JKDownLoaderState state);

// 一个下载器对应一个下载任务
@interface JKDownLoader : NSObject

/**
 文件的总大小
 */
@property(nonatomic,assign) long long expectdContentLength;

/**
 本地已经下载的文件总大小
 */
@property(nonatomic,assign) long long currentContentLength;

/**
 视频的下载地址 URL
 */
@property(nonatomic,strong) NSURL *downloadUrl;

/**
 文件的下载建议保存的名字
 */
@property(nonatomic,strong) NSString *downloadSuggestedFilename;

/**
 下载的进度
 */
@property(nonatomic,assign) float progress;

/**
 当前下载的状态
 */
@property(nonatomic,assign) JKDownLoaderState state;

#pragma mark 当前下载的信息：事件&数据

/**
 下载的信息block
 */
@property(nonatomic,strong) downLoadInfoBlock downInfo;

/**
 当前下载进度的block
 */
@property(nonatomic,strong) downLoadProgressChange downProgress;

/**
 当前下载状态的改变
 */
@property(nonatomic,strong) downLoadStateChangeTypeBlock stateChange;

/**
 下载成功的block
 */
@property(nonatomic,strong) downLoadSuccessBlock success;

/**
 下载失败的block
 */
@property(nonatomic,strong) downLoadFailBlock fail;

#pragma mark 下载接口
/**
 下载接口

 @param url 下载链接的url
 */
-(void)jk_downLoader:(NSURL *)url;

-(void)jk_downLoader:(NSURL *)url withDownInfo:(downLoadInfoBlock)downInfo withProgress:(downLoadProgressChange)progress withdownLoadStateChangeType:(downLoadStateChangeTypeBlock)stateType withSuccess:(downLoadSuccessBlock)success withFail:(downLoadFailBlock)fail;

#pragma mark 暂停当前的下载任务
/**
 暂停当前的下载任务
 */
-(void)jk_pauseCurrentTask;

#pragma mark 取消当前的下载任务
/**
 取消当前的下载任务
 */
-(void)jk_cancelCurrentTask;

#pragma mark 取消并清理当前的下载任务
/**
 取消并清理当前的下载任务
 */
-(void)jk_cancelAndCleanCurrentTask;

#pragma mark 继续当前的下载任务
/**
 继续当前的下载任务
 
 解释：如果调用了几次暂停，就要调用几次继续，才可以就继续
 解决办法：引入状态
 */
-(void)jk_resumeCurrentTask;

#pragma mark 获取下载完成的文件路径
+ (NSString *)downLoadedFileWithURL: (NSURL *)url;
    
    
@end

NS_ASSUME_NONNULL_END
