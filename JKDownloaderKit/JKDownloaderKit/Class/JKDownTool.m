//
//  JKDownTool.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "JKDownTool.h"
#import "JKFilePathExtension.h"
#import "JKDownLoader.h"
@implementation JKDownTool

#pragma mark 下载的状态
/**
 下载的状态
 
 @param fileName 文件的名字
 @return 返回视频下载的状态：1 已下载；2 下载中；3 还没有下载
 */
+(NSString *)downStatusFileName:(NSString *)fileName{

    BOOL isCompletedFileExist = [JKFilePathExtension jk_judgeFileExists:[cachesPathCompleted stringByAppendingPathComponent:fileName]];
    if (isCompletedFileExist) {
        return @"1";
    }

    BOOL isLoadingFileExist = [JKFilePathExtension jk_judgeFileExists:[cachesPathLoading stringByAppendingPathComponent:fileName]];
    if (isLoadingFileExist) {
         return @"2";
    }

    return @"3";
}

@end
