//
//  ViewController.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/9.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "ViewController.h"
#import "JKDownLoaderManger.h"
#import "TestViewController.h"
@interface ViewController ()

@property(nonatomic,strong)JKDownLoaderManger *downLoaderManger;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /**
     下载的思路
     1、下载存放的位置
     1.1、下载中的存放在 tmp 中，设置一个文件夹
     MD5+URL防止资源重复
     1.2、下载完成的存放在 cache 中，设置一个文件夹
     2、判断url地址对应的资源是否下载完毕
     2.1、下载完毕，告诉外界相关信息（本地的路径，文件的大小）return
     2.2、检测临时文件是否存在
     - 不存在，从0字节开始下载 return
     - 存在，
     缓存大小 < 总大小，从该大小开始下载(断点续传)，bytes=字节大小-
     缓存大小 > 总大小，缓存出问题，删除本地缓存，从0开始重新缓存
     缓存大小 = 总大小，缓存完成 ===>> 移动到下载完成的路径中
     
     3、
     
     */
    
    self.view.backgroundColor = [UIColor brownColor];
    
    NSLog(@"家目录=%@",NSHomeDirectory());
    
    UIButton *buttton1 = [[UIButton alloc]initWithFrame:CGRectMake(20, 150, 100, 40)];
    [buttton1 setTitle:@"下载/继续" forState:UIControlStateNormal];
    [buttton1 setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
    [buttton1 setBackgroundColor:[UIColor yellowColor]];
    [buttton1 addTarget:self action:@selector(startLoadTask) forControlEvents:UIControlEventTouchUpInside];
    buttton1.titleLabel.font = [UIFont systemFontOfSize:18.f];
    [self.view addSubview:buttton1];
    
    UIButton *buttton12 = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(buttton1.frame)+20, 150, 100, 40)];
    [buttton12 setTitle:@"暂停" forState:UIControlStateNormal];
    [buttton12 setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
    [buttton12 setBackgroundColor:[UIColor yellowColor]];
    [buttton12 addTarget:self action:@selector(pauseLoadTask) forControlEvents:UIControlEventTouchUpInside];
    buttton12.titleLabel.font = [UIFont systemFontOfSize:18.f];
    [self.view addSubview:buttton12];
    
    UIButton *buttton13 = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(buttton12.frame)+20, 150, 100, 40)];
    [buttton13 setTitle:@"取消" forState:UIControlStateNormal];
    [buttton13 setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
    [buttton13 setBackgroundColor:[UIColor yellowColor]];
    [buttton13 addTarget:self action:@selector(cancelLoadTask) forControlEvents:UIControlEventTouchUpInside];
    buttton13.titleLabel.font = [UIFont systemFontOfSize:18.f];
    [self.view addSubview:buttton13];
    
    UIButton *buttton2 = [[UIButton alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(buttton1.frame)+20, 100, 40)];
    [buttton2 setTitle:@"取消并清理" forState:UIControlStateNormal];
    [buttton2 setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
    [buttton2 setBackgroundColor:[UIColor yellowColor]];
    [buttton2 addTarget:self action:@selector(cancelAndCleanLoadTask) forControlEvents:UIControlEventTouchUpInside];
    buttton2.titleLabel.font = [UIFont systemFontOfSize:18.f];
    [self.view addSubview:buttton2];
    
    self.downLoaderManger = [[JKDownLoaderManger alloc]init];
    
}

#pragma mark 开始下载
-(void)startLoadTask{
    
    NSMutableArray *array = [[NSMutableArray alloc]initWithArray:@[@"http://play.ciotimes.com/DJT55.mp4",@"http://play.ciotimes.com/DJT56.mp4",@"http://play.ciotimes.com/DJT57.mp4"]];
    
    for (NSString *str in array) {
        
        [self.downLoaderManger jk_downLoader:[NSURL URLWithString:str] withDownInfo:^(long long totalSize, NSString * _Nonnull cachePath) {
            
        } withProgress:^(float progress) {
            
            NSLog(@"下载进度=%f",progress);
            
        } withdownLoadStateChangeType:^(JKDownLoaderState state) {
            
            switch (state) {
                case JKDownLoaderStatePause:
                    NSLog(@"暂停");
                    break;
                case JKGDownLoaderStateDowning:
                    NSLog(@"下载中");
                    break;
                case JKGDownLoaderStateSuccess:
                    NSLog(@"下载成功");
                    break;
                case JKGDownLoaderStateFailed:
                    NSLog(@"下载失败");
                    break;
                default:
                    break;
            }
            
        } withSuccess:^(NSString * _Nonnull cachePath) {
            
        } withFail:^(NSString * _Nonnull errorMessage) {
            
        }];
    }
}

#pragma mark 暂停下载
-(void)pauseLoadTask{
    
    [self.downLoaderManger pauseLoadWithUrl:[NSURL URLWithString:@""]];
}

#pragma mark 取消下载
-(void)cancelLoadTask{
    
    [self.downLoaderManger cancelLoadWithUrl:[NSURL URLWithString:@""]];
}

#pragma mark 取消下载并清理缓存
-(void)cancelAndCleanLoadTask{
    
    [self.downLoaderManger cancelAndCleanLoadWithUrl:[NSURL URLWithString:@""]];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    NSLog(@"家目录=%@",NSHomeDirectory());
}

@end
