//
//  TestViewController.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "TestViewController.h"
#import "TestViewCell.h"
#import "DownloadingViewController.h"
#import "DownloadCompletedViewController.h"
#import "TestViewModel.h"
@interface TestViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataArray;

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"视频列表";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"下载中" style:UIBarButtonItemStylePlain target:self action:@selector(clickLeft)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"下载完成" style:UIBarButtonItemStylePlain target:self action:@selector(clickRight)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    // NSArray *array = @[@"http://play.ciotimes.com/DJT55.mp4",@"http://play.ciotimes.com/DJT56.mp4",@"http://play.ciotimes.com/DJT57.mp4"];
    NSArray *array = @[@"http://m2.music.126.net/feplW2VPVs9Y8lE_I08BQQ==/1386484166585821.mp3",@"http://m2.music.126.net/dUZxbXIsRXpltSFtE7Xphg==/1375489050352559.mp3",@"http://m2.music.126.net/zLk1RXSKMONJye6jB3mjSA==/1407374887680902.mp3"];
    
    JKWeakSelf(self);
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        TestViewModel *model = [[TestViewModel alloc]init];
        NSString *url = [NSString stringWithFormat:@"%@",obj];
        model.downUrl = url;
        model.name = url.lastPathComponent;
        model.downStatues = [JKDownTool downStatusFileName:url.lastPathComponent];
        NSLog(@"status=%@",model.downStatues);
        
        [weakself.dataArray addObject:model];
    }];
    
    NSLog(@"数量=%ld 数组=%@",self.dataArray.count,self.dataArray);
    
    [self.view addSubview:self.tableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"TestViewControllerID";
    
    TestViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        
        cell = [[TestViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TestViewModel *model = self.dataArray[indexPath.row];
    
    cell.model = model;
    
    cell.backgroundColor = JKRandomColor;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 13+24+13;
}

-(UITableView *)tableView{

    if (!_tableView) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, JKNaviHeight, CIO_SCREEN_WIDTH, CIO_SCREEN_HEIGHT-JKNaviHeight-JKTabbarHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //self.tableView.bounces = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor  = [UIColor whiteColor];
        if (@available(iOS 11.0, *)) {
            
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            
        } else {
            // 小于11.0的不做操作
            self.automaticallyAdjustsScrollViewInsets = NO;
        }

    }
    
    return _tableView;
}

-(NSMutableArray *)dataArray{
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    
    return _dataArray;
}

-(void)clickLeft{
    
    DownloadingViewController *test1Vc = [DownloadingViewController new];
    [self.navigationController pushViewController:test1Vc animated:YES];
}

-(void)clickRight{
    
    DownloadCompletedViewController *test2Vc = [DownloadCompletedViewController new];
    [self.navigationController pushViewController:test2Vc animated:YES];
}


@end
