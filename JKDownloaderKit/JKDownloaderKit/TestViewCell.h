//
//  TestViewCell.h
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestViewModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    // 还没下载(等待下载)
    JKDownCellStateWaitDownLoad,
    // 下载中
    JKDownCellStateDownLoading,
    // 下载完成
    JKDownCellStateDownLoaded,
} JKDownCellState;

@interface TestViewCell : UITableViewCell

/**
 视频的名字
 */
@property(nonatomic,strong) UILabel *videoName;

/**
 视频的暂停与开始下载按钮
 */
@property(nonatomic,strong) UIButton *videoSwitch;

/**
 下载的传递
 */
@property(nonatomic,strong) void(^downBlock)(void);
    
    
@property(nonatomic,strong) TestViewModel *model;

@end

NS_ASSUME_NONNULL_END
