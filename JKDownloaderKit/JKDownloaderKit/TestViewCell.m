//
//  TestViewCell.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "TestViewCell.h"

@interface TestViewCell ()

@property (nonatomic, assign) JKDownCellState state;

@end

@implementation TestViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    // 0. 添加监听下载状态的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downLoadStateChange:) name:JKDownLoadURLOrStateChangeNotification object:nil];
    
    if (self) {
        
        [self.contentView addSubview:self.videoName];
        [self.contentView addSubview:self.videoSwitch];
    }
    
    return self;
}
    
/**
  当下载状态发生变更时调用
     
  @param notice 通知, 内部包含url 和 下载状态
 
 */
- (void)downLoadStateChange: (NSNotification *)notice {
    
    NSDictionary *downDic = notice.userInfo;
    NSURL *url = downDic[@"downLoadURL"];
    
    NSURL *currentUrl = [NSURL URLWithString:self.model.downUrl];
    
    if ([url isEqual:currentUrl]) {
        
        JKDownLoaderState state = [downDic[@"downLoadState"] integerValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self changeStatus:state withUrl:url];
        });

    }
    
}

-(UILabel *)videoName{
    
    if (!_videoName) {
        
        _videoName = [[UILabel alloc]initWithFrame:CGRectMake(20, 13, CIO_SCREEN_WIDTH-20-100, 24)];
        _videoName.font = [UIFont systemFontOfSize:22.f];
        _videoName.textColor = [UIColor whiteColor];
    }
    return _videoName;
}

-(UIButton *)videoSwitch{
    
    if (!_videoSwitch) {
        
        _videoSwitch = [[UIButton alloc]initWithFrame:CGRectMake(CIO_SCREEN_WIDTH-90, 13, 80, 24)];
        _videoSwitch.titleLabel.font = [UIFont systemFontOfSize:22.f];
        [_videoSwitch setTitle:@"下载" forState:UIControlStateNormal];
        _videoSwitch.backgroundColor = [UIColor whiteColor];
        [_videoSwitch setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
        [_videoSwitch addTarget:self action:@selector(downLoad) forControlEvents:UIControlEventTouchUpInside];
    }
    return _videoSwitch;
}

-(void)downLoad{
    
    if (self.state == JKDownCellStateWaitDownLoad) {
        NSLog(@"下载");
        // 事件触发
        NSURL *url = [NSURL URLWithString:self.model.downUrl];
      
        [[JKDownLoaderManger shareDownloaderManger]jk_downLoader:url withDownInfo:nil withProgress:^(float progress){
            
            NSLog(@"下载进度=%f",progress);
            
        }  withdownLoadStateChangeType:nil withSuccess:nil withFail:nil];
    }
}

-(void)setState:(JKDownCellState)state{
    
    _state = state;
    
    switch (state) {
        case JKDownCellStateWaitDownLoad:
        NSLog(@"等待下载");
    
        [self.videoSwitch setTitle:@"下载" forState:UIControlStateNormal];
        break;
        case JKDownCellStateDownLoading:
        {
            NSLog(@"正在下载");
            [self.videoSwitch setTitle:@"下载中" forState:UIControlStateNormal];
            
            break;
        }
        case JKDownCellStateDownLoaded:
        NSLog(@"下载完毕");
        
        [self.videoSwitch setTitle:@"已下载" forState:UIControlStateNormal];
        
        break;
        default:
        break;
    }
}
    
-(void)setModel:(TestViewModel *)model{
    
    _model = model;
    
    self.videoName.text = model.name;
    
    /**
     1、下载完成：先去下载完成里面查看有没有
     2、下载中：再去下载中查看
     3、下载：还没有下载
     */
    
    // 一定要注意动态的获取下载状态
    // 因为cell重用, 所以必须准确的获取url对应的下载状态
    NSURL *url = [NSURL URLWithString:self.model.downUrl];
    JKDownLoader *downLoader = [[JKDownLoaderManger shareDownloaderManger] getDownLoaderWithURL:url];
    
    JKDownLoaderState state = downLoader.state;
    
    [self changeStatus:state withUrl:url];
}
    
-(void)changeStatus:(JKDownLoaderState)state withUrl:(NSURL *)url{
    
    if (state == JKGDownLoaderStateDowning) {
        self.state = JKDownCellStateDownLoading;
    }else if(state == JKGDownLoaderStateSuccess || [JKDownLoader downLoadedFileWithURL:url].length > 0)
    {
        self.state = JKDownCellStateDownLoaded;
    }else {
        self.state = JKDownCellStateWaitDownLoad;
    }
}
    
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
