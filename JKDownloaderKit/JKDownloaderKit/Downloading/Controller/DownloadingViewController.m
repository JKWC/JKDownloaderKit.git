//
//  DownloadingViewController.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "DownloadingViewController.h"
#import "DownloadingViewCell.h"
#import "TestViewModel.h"
@interface DownloadingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSMutableArray *dataArray;


@end

@implementation DownloadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"下载中";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSLog(@"家目录=%@",NSHomeDirectory());
    
    [self.dataArray addObjectsFromArray:[JKFilePathExtension jk_fileList:cachesPathLoading Isascending:YES]];
    
    NSLog(@"数组的数量=%ld\n 内容=%@",self.dataArray.count,self.dataArray);
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"TestViewControllerID";
    
    DownloadingViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        
        cell = [[DownloadingViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TestViewModel *model = self.dataArray[indexPath.row];
    
    cell.backgroundColor = JKRandomColor;
    
    cell.videoName.text = model.name;
    
    JKWeakSelf(self);
    cell.downBlock = ^{
        
        if ([model.downStatues isEqualToString:@"3"]) {
            
            [weakself downClickRowAtIndexPath:indexPath];
        }
    };
    
    /**
     1、下载完成：先去下载完成里面查看有没有
     2、下载中：再去下载中查看
     3、下载：还没有下载
     */
    if ([model.downStatues isEqualToString:@"1"]) {
        
        [cell.videoSwitch setTitle:@"已下载" forState:UIControlStateNormal];
    }else if ([model.downStatues isEqualToString:@"2"]){
        
        [cell.videoSwitch setTitle:@"下载中" forState:UIControlStateNormal];
    }else if ([model.downStatues isEqualToString:@"3"]){
        
        [cell.videoSwitch setTitle:@"下载" forState:UIControlStateNormal];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 13+24+8+14+13;
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, JKNaviHeight, CIO_SCREEN_WIDTH, CIO_SCREEN_HEIGHT-JKNaviHeight-JKTabbarHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //self.tableView.bounces = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor  = [UIColor whiteColor];
        if (@available(iOS 11.0, *)) {
            
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            
        } else {
            // 小于11.0的不做操作
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        
    }
    
    return _tableView;
}

-(NSMutableArray *)dataArray{
    
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc]init];
    }
    
    return _dataArray;
}

#pragma mark 下载的点击事件
-(void)downClickRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"点击的第 %ld 个",indexPath.row);
    
    TestViewModel *model = self.dataArray[indexPath.row];
    model.downStatues = @"2";
    [self.tableView reloadData];
    
    [[JKDownLoaderManger shareDownloaderManger]jk_downLoader:[NSURL URLWithString:model.downUrl] withDownInfo:^(long long totalSize, NSString * _Nonnull cachePath) {
        
    } withProgress:^(float progress) {
        
        // NSLog(@"下载的进度=%f",progress);
        model.progress = [NSString stringWithFormat:@"%.f%%",progress*100];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSIndexPath *indexPath1=[NSIndexPath indexPathForRow:indexPath.row inSection:0];
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1,nil] withRowAnimation:UITableViewRowAnimationNone];
            
        });
        
    } withdownLoadStateChangeType:^(JKDownLoaderState state) {
        
    } withSuccess:^(NSString * _Nonnull cachePath) {
        
        NSLog(@"下载完成");
        model.downStatues = @"1";
        [self.tableView reloadData];
        
        
    } withFail:^(NSString * _Nonnull errorMessage) {
        
    }];
    
}


@end
