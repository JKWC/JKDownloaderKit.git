//
//  DownloadingViewCell.m
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import "DownloadingViewCell.h"

@implementation DownloadingViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self.contentView addSubview:self.videoName];
        [self.contentView addSubview:self.videoSwitch];
        [self.contentView addSubview:self.progressLabel];
    }
    
    return self;
}

-(UILabel *)videoName{
    
    if (!_videoName) {
        
        _videoName = [[UILabel alloc]initWithFrame:CGRectMake(20, 13, CIO_SCREEN_WIDTH-20-100, 24)];
        _videoName.font = [UIFont systemFontOfSize:22.f];
        _videoName.textColor = [UIColor whiteColor];
    }
    return _videoName;
}

-(UIButton *)videoSwitch{
    
    if (!_videoSwitch) {
        
        _videoSwitch = [[UIButton alloc]initWithFrame:CGRectMake(CIO_SCREEN_WIDTH-90, 13, 80, 24)];
        _videoSwitch.titleLabel.font = [UIFont systemFontOfSize:22.f];
        [_videoSwitch setTitle:@"下载" forState:UIControlStateNormal];
        _videoSwitch.backgroundColor = [UIColor whiteColor];
        [_videoSwitch setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
        [_videoSwitch addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    }
    return _videoSwitch;
}

-(UILabel *)progressLabel{
    
    if (!_progressLabel) {
        
        _progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 13+24+8, CIO_SCREEN_WIDTH-20-100, 16)];
        _progressLabel.font = [UIFont systemFontOfSize:14.f];
        _progressLabel.textColor = [UIColor whiteColor];
        _progressLabel.text = @"0%";
    }
    return _progressLabel;
}


-(void)click{
    
    if (self.downBlock) {
        
        self.downBlock();
    }
    
}



@end
