//
//  DownloadingViewCell.h
//  JKDownloaderKit
//
//  Created by 王冲 on 2019/2/13.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DownloadingViewCell : UITableViewCell

/**
 视频的名字
 */
@property(nonatomic,strong) UILabel *videoName;

/**
 视频的暂停与开始下载按钮
 */
@property(nonatomic,strong) UIButton *videoSwitch;

/**
 视频下载进度的label
 */
@property(nonatomic,strong) UILabel *progressLabel;

/**
 下载的传递
 */
@property(nonatomic,strong) void(^downBlock)(void);

@end

NS_ASSUME_NONNULL_END
